public class Task5130 {
    public static void main(String[] args) {
        System.out.println("st1");
        String inputst1 = "bananas";
        String resultst1 = removeDuplicates(inputst1);
        System.out.println(resultst1);
        String inputst12 = "devcamp";
        String resultst12 = removeDuplicates(inputst12);
        System.out.println(resultst12);

        System.out.println("st2");

        String inputst2 = "Devcamp java";
        int resultst2 = kyTuKhongKhoanTrang(inputst2);
        System.out.println(resultst2);
        String inputst22 = "Abc  abc bac";
        int resultst22 = kyTuKhongKhoanTrang(inputst22);
        System.out.println(resultst22);

        System.out.println("st3");
        String inputst3 = "abc";
        String inputst32 = "def";
        String resultst3 = inputst3 + inputst32;
        System.out.println(resultst3);

        System.out.println("st4");
        String s1 = "Devcamp java";
        String s2 = "java";
        boolean contains = s1.contains(s2); // kiểm tra xem s1 có chứa s2 hay không
        if (contains) {
            System.out.println("s1 chứa s2" + "true");
        } else {
            System.out.println("s1 chứa s2" + "false");
        }

        System.out.println("st5");
        String inputst5 = "Devcamp";
        int k = 3;
        char character = inputst5.charAt(k);
        System.out.println(character);

        System.out.println("st6");
        String inputst6 = "Devcamp java";
        int resultst6 = kyTuHienTrongChuoi(inputst6);
        System.out.println(resultst6);

        System.out.println("st7");
        String inputst7 = "Devcamp java";
        char targetChar = 'a';
        int firstOccurrence = inputst7.indexOf(targetChar); // Tìm vị trí xuất hiện lần đầu tiên của ký tự
        System.out.println(firstOccurrence);

        System.out.println("st8");
        String inputst8 = "Devcamp";
        String inHoa = inputst8.toUpperCase();
        System.out.println(inHoa);

        System.out.println("st9");
        String inputst9 = "DevCamp";
        String resultst9 = chuyenKyTu(inputst9);
        System.out.println(resultst9);

        System.out.println("st10");
        String inputst10 = "DevCamp";
        int resultst10 = soKyTuInHoa(inputst10);
        System.out.println(resultst10);
        String inputst102 = "DevCamp123";
        int resultst102 = soKyTuInHoa(inputst102);
        System.out.println(resultst102);

        System.out.println("st11");

        String inputst112 = "DEVCAMP123";
        for (int i = 0; i < inputst112.length(); i++) {
            char c = inputst112.charAt(i);
            if (Character.isUpperCase(c)) {
                System.out.println("Ký tự in hoa: " + c);
            }
        }

        System.out.println("st12");
        String str = "abcdefghijklmnopqrstuvwxy";
        int n = 5;

        String[] parts = divideString(str, n);

        if (parts == null) {
            System.out.println("KO");
        } else {
            for (String part : parts) {
                System.out.println(part);
            }
        }

        System.out.println("st13");
        String input = "aabaarbarccrabmq";
        String result = removeDuplicates(input);
        System.out.println("Chuỗi sau khi xóa các ký tự giống nhau: " + result);

        System.out.println("st14");
        String inputst14 = "Welcome";
        String inputst142 = "home";
        String resultWelcom = inputst14.substring(3);
        System.out.println(resultWelcom);
        String resultSt2 = resultWelcom + inputst142;
        System.out.println(resultSt2);

        System.out.println("st15");
        String inputSt3 = "DevCamp";
        String resultSt3 = chuyenKyTu(inputSt3);
        System.out.println(resultSt3);

        System.out.println("st16");
        String inputst4 = "DevCamp";
        boolean coSo = kiemTraChuoi(inputst4);
        System.out.println(coSo);

        String inputst42 = "Devcamp123";
        boolean khongCoSo = kiemTraChuoi(inputst42);
        System.out.println(khongCoSo);

        System.out.println("st17");
        String inputst17 = "DevCamp123";
        String inputst172 = "DEVCAMP 123";
        if(checkString(inputst172)){
            System.out.println("false.");
        }else {
            System.out.println("true");
        }

        if (checkString(inputst17)) {
            System.out.println("false");
        } else {
            System.out.println("true");
        }
    };
    public static boolean checkString(String str) {
        String regex = "^[A-Z].*[0-9]$";
        
        return str.length() <= 20 && !str.contains(" ") && str.matches(regex);
    }

    public static String[] divideString(String str, int n) {
        if (str.length() % n != 0) {
            return null;
        }

        int numParts = str.length() / n;
        String[] parts = new String[numParts];

        for (int i = 0; i < numParts; i++) {
            parts[i] = str.substring(i * n, (i + 1) * n);
        }

        return parts;
    }

    public static int soKyTuInHoa(String input) {
        int dem = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isUpperCase(c)) {
                dem++;
            }
        }
        return dem;
    }

    public static int kyTuHienTrongChuoi(String input) {
        char c = 'a';
        int dem = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == c) {
                dem++;
            }
        }
        return dem;
    }

    public static int kyTuKhongKhoanTrang(String input) {
        int soLuongTu = 0;
        boolean isWord = false;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != ' ' && !isWord) {
                soLuongTu++;
                isWord = true;
            } else if (c == ' ') {
                isWord = false;
            }
        }
        return soLuongTu;
    };

    public static boolean kiemTraChuoi(String chuoi) {
        for (int i = 0; i < chuoi.length(); i++) {
            if (Character.isDigit(chuoi.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static String chuyenKyTu(String input) {
        String result = "";

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isUpperCase(c)) {
                result += Character.toLowerCase(c);
            } else if (Character.isLowerCase(c)) {
                result += Character.toUpperCase(c);
            } else {
                result += c;
            }
        }
        return result;
    }

    public static String removeDuplicates(String input) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            if (output.indexOf(String.valueOf(currentChar)) == -1) {
                output.append(currentChar);
            }
        }
        return output.toString();
    }
}
