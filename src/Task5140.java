import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Task5140 {
    public static void main(String[] args) {
        System.out.println("st1");
        // tạo một arrayList có tên là arrListSt1
        // có kiểu là integer
        ArrayList<Integer> arrListSt1 = new ArrayList<Integer>();
        arrListSt1.add(29);
        arrListSt1.add(12);
        arrListSt1.add(19);
        arrListSt1.add(95);
        arrListSt1.add(27);
        arrListSt1.add(6);
        arrListSt1.add(12);
        arrListSt1.add(30);
        arrListSt1.add(25);
        arrListSt1.add(23);
        System.out.println(arrListSt1);
        Collections.sort(arrListSt1);
        System.out.println(arrListSt1);

        System.out.println("st2");
        ArrayList<Integer> newArrList = new ArrayList<Integer>();
        for (Integer num : arrListSt1) {
            if (num >= 10 && num <= 100) {
                newArrList.add(num);
            }
        }

        System.out.println("ArrayList moi chua cac so tu 10 den 100:");
        System.out.println(newArrList);

        System.out.println("st3");
        ArrayList<String> arrListStringColor = new ArrayList<String>();
        arrListStringColor.add("white");
        arrListStringColor.add("green");
        arrListStringColor.add("blue");
        arrListStringColor.add("purple");
        arrListStringColor.add("orange");

        System.out.println(arrListStringColor);
        if (arrListStringColor.contains("yellow")) {
            System.out.println("OK");
        } else {
            System.out.println("NO");
        }

        System.out.println("st4");

        int tong = 0;
        for (int i = 0; i < arrListSt1.size(); i++) {
            tong += arrListSt1.get(i);
        }
        System.out.println("Tong cac so cua arraylist: " + tong);

        System.out.println("st5");
        arrListStringColor.clear();
        System.out.println(arrListStringColor);

        System.out.println("st6");
        ArrayList<String> arrListColor = new ArrayList<String>();
        arrListColor.add("white");
        arrListColor.add("green");
        arrListColor.add("blue");
        arrListColor.add("purple");
        arrListColor.add("orange");
        arrListColor.add("red");
        arrListColor.add("yello");
        arrListColor.add("violet");
        arrListColor.add("pink");
        arrListColor.add("gray");
        System.out.println(arrListColor);

        Collections.shuffle(arrListColor);
        System.out.println("ArrayList sau khi hoan doi: " + arrListColor);

        System.out.println("st7");

        Collections.reverse(arrListColor);
        System.out.println("ArrayList sau khi dao nguoc" + arrListColor);

        System.out.println("st8");
        //cắt phần tử thứ 3 đến phần tử thứ 7 của arraylistcolor
        List<String> newArrayColor = arrListColor.subList(3, 7);
        System.out.println("Array list moi sau khi cat" + newArrayColor);

        System.out.println("st9");
        Collections.swap(arrListColor, 3, 7);
        System.out.println("Array list sau khi hoan doi" + arrListColor);

        System.out.println("st10");
        ArrayList<Integer> arrList1 = new ArrayList<Integer>();
        ArrayList<Integer> arrList2 = new ArrayList<Integer>();
        arrList1.add(12);
        arrList1.add(45);
        arrList1.add(27);
        arrList2.addAll(arrList1);
        System.out.println("ArrayList 1: " + arrList1);
        System.out.println("ArrayList 2 sau khi thêm: " + arrList2);
    }
}
