import java.util.ArrayList;
import java.util.Iterator;
public class Task5120 {
    public static void main(String[] args) {
        System.out.println("st1");
        // khai báo một ArrayList có tên là List
        // có kiểu là String
        ArrayList<String> list = new ArrayList<String>();
        // thêm các phần tử vào list
        list.add("white");
        list.add("green");
        list.add("blue");
        list.add("purple");
        list.add("orange");
        // hiển thị các phần tử của list
        System.out.println("Các phần tử có trong list là: ");
        System.out.println(list);

        System.out.println("st2");
        // khai báo hai Arraylist có tên là arrListInteger, arrListIntegerPlus
        // có kiểu là integer
        ArrayList<Integer> arrListInteger = new ArrayList<Integer>();
        ArrayList<Integer> arrListIntegerPlus = new ArrayList<Integer>();
        // thêm các phần tử sử dụng phương thức add
        arrListInteger.add(1);
        arrListInteger.add(2);
        arrListInteger.add(3);
        // thêm các phần tử cảu arrListInteger vào arrListIntegerPlus
        arrListIntegerPlus.addAll(0, arrListInteger);
        // In ra ArrayList sau khi được cộng thêm
        System.out.println("ArrayList sau khi cộng thêm: " + arrListIntegerPlus);

        System.out.println("st3");
        // khai báo một ArrayList có tên là List
        // có kiểu là String
        ArrayList<String> arrListColor = new ArrayList<String>();
        // thêm các phần tử vào list
        arrListColor.add("white");
        arrListColor.add("green");
        arrListColor.add("blue");
        arrListColor.add("purple");
        arrListColor.add("orange");

        int count = arrListColor.size();
        System.out.println("Số lượng phần tử của ArrayList: " + count);

        System.out.println("st4");
        ArrayList<String> arrListColorSt4 = new ArrayList<String>();
        // thêm các phần tử vào list
        arrListColorSt4.add("white");
        arrListColorSt4.add("green");
        arrListColorSt4.add("blue");
        arrListColorSt4.add("purple");
        arrListColorSt4.add("orange");

        String element = arrListColorSt4.get(3);
        System.out.println("Số lượng phần tử của ArrayList: " + element);

        System.out.println("st5");
        String lastElement = arrListColorSt4.get(arrListColorSt4.size()-1);
        System.out.println("phần tử cuối của ArrayList: " + lastElement);

        System.out.println("st6");
        arrListColorSt4.remove(4);
        System.out.println(arrListColorSt4);

        System.out.println("st7");
        for(String str: arrListColorSt4){
            System.out.println(str);
        }

        System.out.println("st8");
        // sử dụng Iterator - hiển thị các phần tử của list
        Iterator<String> iterator = arrListColorSt4.iterator();
        System.out.println("Các phần tử có trong list là: ");
        while (iterator.hasNext()) {
            System.out.println((String) iterator.next());
        }

        System.out.println("st9");
        for (int index = 0; index < arrListColorSt4.size(); index++) {
            System.out.println(arrListColorSt4.get(index));
        }

        System.out.println("st10");
        System.out.println(arrListColorSt4);
        arrListColorSt4.add(0,"vaiolet");
        System.out.println(arrListColorSt4);

        System.out.println("st11");
        System.out.println(arrListColorSt4);
        arrListColorSt4.set(2, "red");
        System.out.println(arrListColorSt4);

        System.out.println("st12");
        //tạo một ArrayListString có tên là arrListName
        //có kiểu là string
        ArrayList<String> arrListName = new ArrayList<String>();
        //thêm các phần tử vào trong list
        arrListName.add("John");
        arrListName.add("Alice");
        arrListName.add("Bob");
        arrListName.add("Steve");
        arrListName.add("John");
        arrListName.add("Steve");
        arrListName.add("Maria");
        // kiểm tra xem Alice có tồn tại trong list hay không?
        System.out.println(arrListName.indexOf("Alice"));
        // kiểm tra xem Mark có tồn tại trong list hay không?
        System.out.println(arrListName.indexOf("Mark"));
        
        System.out.println("st13");
        // kiểm tra xem Steve có tồn tại trong list hay không?
        System.out.println(arrListName.lastIndexOf("Steve"));
        // kiểm tra xem John có tồn tại trong list hay không?
        System.out.println(arrListName.lastIndexOf("John"));

    }
}