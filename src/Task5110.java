import java.util.HashMap;
import java.util.Map;

public class Task5110 {
    public static void main(String[] args) throws Exception {
        System.out.println("st1");
        String str = "Java";
        str = str.toLowerCase();

        int[] charCount = new int[26]; // Mảng đếm số lần xuất hiện của mỗi ký tự
        int maxCount = 0;
        int maxIndex = -1;

        for (char ch : str.toCharArray()) {
            if (Character.isLetter(ch)) {
                int index = ch - 'a';
                charCount[index]++;

                if (charCount[index] > maxCount) {
                    maxCount = charCount[index];
                    maxIndex = index;
                }
            }
        }

        if (maxCount == 1) {
            System.out.println("NO");
        } else {
            char mostFrequentChar = (char) (maxIndex + 'a');
            System.out.println(mostFrequentChar);
        }

        String chuoist12 = "Devcamp JAVA exercise";
        chuoist12 = chuoist12.toLowerCase();
        int[] charCountst12 = new int[26];
        int maxCountst12 = 0;
        int maxIndexst12 = -1;
        for (char ch : chuoist12.toCharArray()) {
            if (Character.isLetter(ch)) {
                int index = ch - 'a';
                charCount[index]++;

                if (charCount[index] > maxCount) {
                    maxCountst12 = charCountst12[index];
                    maxIndexst12 = index;
                }
            }
        }

        if (maxCountst12 == 1) {
            System.out.println("NO");
        } else {
            char mostFrequentChar = (char) (maxIndexst12 + 'a');
            System.out.println(mostFrequentChar);
        }

        String chuoist123 = "Devcamp";
        chuoist123 = chuoist123.toLowerCase();
        int[] charCountst123 = new int[26];
        int maxCountst123 = 0;
        int maxIndexst123 = -1;
        boolean isUnique = true;

        for (char ch : chuoist123.toCharArray()) {
            if (Character.isLetter(ch)) {
                int index = ch - 'a';
                charCountst123[index]++;

                if (charCountst123[index] > maxCountst123) {
                    maxCountst123 = charCountst123[index];
                    maxIndexst123 = index;
                    isUnique = false;
                } else if (charCountst123[index] == maxCountst123) {
                    isUnique = true;
                }
            }
        }

        if (isUnique) {
            System.out.println("NO");
        } else {
            char mostFrequentChar = (char) (maxIndexst123 + 'a');
            System.out.println(mostFrequentChar);
        }

        System.out.println("st2");
        String chuoi1st2 = "word";
        String chuoi2st2 = "drow";

        String chuoi2DaoNguoc = new StringBuilder(chuoi2st2).reverse().toString();

        // So sánh chuỗi thứ nhất với chuỗi thứ hai đã đảo ngược
        if (chuoi1st2.equals(chuoi2DaoNguoc)) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        String chuoi1st22 = "Java";
        String chuoi2st22 = "js";

        String chuoi2DaoNguocSt22 = new StringBuilder(chuoi2st22).reverse().toString();

        // so sánh chuổi thú nhất với chuổi đã đảo ngược
        if (chuoi1st22.equals(chuoi2DaoNguocSt22)) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        System.out.println("st3");
        String chuoist3 = "Java";
        // Chuyển chuỗi thành mảng các ký tự
        char[] characters = chuoist3.toCharArray();

        // Sử dụng HashMap để đếm số lần xuất hiện của từng ký tự
        Map<Character, Integer> charCountMap = new HashMap<>();

        for (char ch : characters) {
            // Tăng giá trị đếm của ký tự
            charCountMap.put(ch, charCountMap.getOrDefault(ch, 0) + 1);
        }

        // Tìm ký tự đầu tiên chỉ xuất hiện một lần
        char uniqueChar = '\0';
        for (char ch : characters) {
            if (charCountMap.get(ch) == 1) {
                uniqueChar = ch;
                break;
            }
        }

        // Xuất kết quả
        if (uniqueChar != '\0') {
            System.out.println(uniqueChar);
        } else {
            System.out.println("NO");
        }

        String chuoiHaha = "Haha";
        String chuoiLowerCase = chuoiHaha.toLowerCase();
        char[] charactersHaha = chuoiLowerCase.toCharArray();

        // Sử dụng HashMap để đếm số lần xuất hiện của từng ký tự
        Map<Character, Integer> charCountMapHaha = new HashMap<>();
        for (char ch : charactersHaha) {
            charCountMapHaha.put(ch, charCountMapHaha.getOrDefault(ch, 0) + 1);
        }

        // Tìm ký tự chỉ xuất hiện một lần và xuất kết quả
        char uniqueCharHaha = '\0';
        for (char ch : charactersHaha) {
            if (charCountMapHaha.get(ch) == 1) {
                uniqueCharHaha = ch;
                break;
            }
        }

        // Xuất kết quả
        if (uniqueCharHaha != '\0') {
            System.out.println(uniqueCharHaha);
        } else {
            System.out.println("NO");
        }

        String chuoist33 = "Devcamp";
        char[] charactersst33 = chuoist33.toCharArray();

        Map<Character, Integer> charCountMapst33 = new HashMap<>();

        for (char ch : charactersst33) {
            charCountMapst33.put(ch, charCountMapst33.getOrDefault(ch, 0) + 1);
        }

        char uniqueCharst33 = '\0';
        for (char ch : charactersst33) {
            if (charCountMapst33.get(ch) == 1) {
                uniqueCharst33 = ch;
                break;
            }
        }

        if (uniqueCharst33 != '\0') {
            System.out.println(uniqueCharst33);
        } else {
            System.out.println("NO");
        }

        System.out.println("st4");

        String chuoist4 = "word";
        StringBuilder sb = new StringBuilder(chuoist4);
        String chuoiDaoNguoc = sb.reverse().toString();
        System.out.println(chuoiDaoNguoc);

        String chuoist42 = "java";
        StringBuffer sd = new StringBuffer(chuoist42);
        String chuoiDaoNguocSt42 = sd.reverse().toString();
        System.out.println(chuoiDaoNguocSt42);

        System.out.println("st5");
        String chuoiSt5 = "abc";
        boolean containsDigits = coSoKhong(chuoiSt5);
        boolean hasNoDigits = !containsDigits;

        System.out.println(hasNoDigits);

        String chuoiSt52 = "a1bc";
        boolean chuoiKhongCoSo = coSoKhong(chuoiSt52);
        boolean chuoiCoSo = !chuoiKhongCoSo;

        System.out.println(chuoiCoSo);

        System.out.println("st6");
        String chuoist6 = "java";
        String chuoist62 = "devcamp";
        int vowelCount = demNguyenAm(chuoist6);
        int vowelCount2 = demNguyenAm(chuoist62);
        System.out.println("Số lượng ký tự nguyên âm: " + vowelCount);
        System.out.println("Số lượng ký tự nguyên âm: " + vowelCount2);

        System.out.println("st7");
        String chuoist7 = "1234";
        // Sử dụng parseInt()
        int giaTri1 = Integer.parseInt(chuoist7);
        System.out.println("Giá trị sử dụng parseInt(): " + giaTri1);

        // Sử dụng valueOf()
        Integer giaTri2 = Integer.valueOf(chuoist7);
        System.out.println("Giá trị sử dụng valueOf(): " + giaTri2);

        System.out.println("st8");
        String chuoist8 = "devcamp java";
        char oldChar = 'a';
        char newChar = 'b';

        String replacedStr = chuoist8.replace(oldChar, newChar);
        System.out.println("Chuỗi đã chuyển đổi: " + replacedStr);

        String chuoist82 = "exercise";
        char oldChar2 = 'e';
        char newChar2 = 'f';
        String thayTheKyTuChuoi = chuoist82.replace(oldChar2, newChar2);
        System.out.println(thayTheKyTuChuoi);

        System.out.println("st9");
        String chuoist9 = "I am developer";
        String[] words = chuoist9.split(" ");

        StringBuilder reversedStrBuilder = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            reversedStrBuilder.append(words[i]);

            if (i > 0) {
                reversedStrBuilder.append(" ");
            }
        }

        String reversedStr = reversedStrBuilder.toString();
        System.out.println("Chuỗi đã đảo ngược: " + reversedStr);

        System.out.println("st10");
        String chuoist10 = "aba";
        String reversedStrst10 = reverseString(chuoist10);

        boolean isPalindrome = chuoist10.equals(reversedStrst10);

        System.out.println("Kết quả kiểm tra: " + isPalindrome);

        String chuoist102 = "abc";
        String chuoiDaoNguocst102 = reverseString(chuoist102);

        boolean isPalindromest102 = chuoist102.equalsIgnoreCase(chuoiDaoNguocst102);

        System.out.println("Kết quả kiểm tra: " + isPalindromest102);
    }

    public static String reverseString(String str) {
        StringBuilder reversedStrBuilder = new StringBuilder(str);
        reversedStrBuilder.reverse();
        return reversedStrBuilder.toString();
    }

    public static int demNguyenAm(String chuoi) {
        int count = 0;
        chuoi = chuoi.toLowerCase();

        for (int i = 0; i < chuoi.length(); i++) {
            char ch = chuoi.charAt(i);
            if (isNguyenAm(ch)) {
                count++;
            }
        }

        return count;
    }

    public static boolean isNguyenAm(char ch) {
        ch = Character.toLowerCase(ch);
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }

    public static boolean coSoKhong(String chuoi) {
        boolean containsDigits = chuoi.matches(".*\\d.*");
        return containsDigits;
    }
}
